package org.testng.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Marks a method as a factory that returns objects that will be used by TestNG
 * as Test classes.  The method must return Object[].
 *
 * @author <a href="mailto:cedric&#64;beust.com">Cedric Beust</a>
 */

@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.METHOD, java.lang.annotation.ElementType.CONSTRUCTOR})
public @interface Factory {
  /**
   * The list of variables used to fill the parameters of this method.
   * These variables must be defined in the property file.
   *
   * @deprecated Use @Parameters
   */
  @Deprecated
  public String[] parameters() default {};

  /**
   * The name of the data provider for this test method.
   * @see org.testng.annotations.DataProvider
   */
  public String dataProvider() default "";

  /**
   * The class where to look for the data provider.  If not
   * specified, the dataprovider will be looked on the class
   * of the current test method or one of its super classes.
   * If this attribute is specified, the data provider method
   * needs to be static on the specified class.
   */
  public Class<?> dataProviderClass() default Object.class;

  /**
   * The CSV resource that is used as a data provider for this test method.
   * It can be its name, in which case the resource associated with the test class will be loaded, or its absolute URL.
   */
  public String dataProviderCsv() default "";

  /**
   * If set to true, the first line the CSV file that is used as data provider
   * will be considered a header and not a test scenario.
   * Only used by CSV-based data providers.
   */
  public boolean csvHeader() default false;

  /**
   * The string used as element separator in array-valued columns of a CSV.
   * Only used by CSV-based data providers.
   */
  public String csvArrayElementSeparator() default ",";

  /**
   * Whether this factory is enabled.
   */
  public boolean enabled() default true;
}
