import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CSVTestWithDataProvider {

    private Subject subject = new Subject();

    @Test(dataProviderCsv = "/sumDataProvider_WithHeader.csv", csvHeader = true)
    public void testSubjectWithCSVHeader(int a, int b, int expectedSum) {
        assertEquals(subject.sum(a, b), expectedSum);
    }

    @Test(dataProviderCsv = "/sumDataProvider_NoHeader.csv")
    public void testSubjectNoCSVHeader(int a, int b, int expectedSum) {
        assertEquals(subject.sum(a, b), expectedSum);
    }

    @Test(dataProviderCsv = "/sumDataProvider_List_WithHeader.csv", csvHeader = true)
    public void testSubjectListWithHeader(int[] numbers, int expectedSum) {
        assertEquals(subject.sum(numbers), expectedSum);
    }

    @Test(dataProviderCsv = "/sumDataProvider_List_WithHeaderAndCustomSeparator.csv", csvHeader = true, csvArrayElementSeparator = "|")
    public void testSubjectListWithHeaderAndCustomArrayElementSeparator(int[] numbers, int expectedSum) {
        assertEquals(subject.sum(numbers), expectedSum);
    }
}
