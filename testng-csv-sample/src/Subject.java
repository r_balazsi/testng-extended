
public class Subject {

	public int sum(int... numbers) {
    int s = 0;
		for (int number : numbers) {
      s += number;
    }

    return s;
	}
}
