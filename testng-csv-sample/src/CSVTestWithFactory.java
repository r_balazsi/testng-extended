import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CSVTestWithFactory {

  private int[] numbers;
  private int expectedSum;

  private Subject subject = new Subject();

  @Factory(dataProviderCsv = "/sumDataProvider_List_WithHeaderAndCustomSeparator.csv", csvHeader = true, csvArrayElementSeparator = "|")
  public CSVTestWithFactory(int[] numbers, int expectedSum) {
    this.numbers = numbers;
    this.expectedSum = expectedSum;
  }

  @Test
  public void testSubject() {
    assertEquals(subject.sum(numbers), expectedSum);
  }
}
